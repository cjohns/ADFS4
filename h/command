/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * cddl/RiscOS/Sources/FileSys/ADFS/ADFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2015 Ben Avison.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef COMMAND_H
#define COMMAND_H

#include <stdarg.h>
#include "kernel.h"

_kernel_oserror *command_adfs(void);
#ifdef ENABLE_BACKGROUND_TRANSFERS
_kernel_oserror *command_configure_syntax_adfsbuffers(void);
_kernel_oserror *command_status_adfsbuffers(void);
_kernel_oserror *command_syntax_adfsbuffers(void);
_kernel_oserror *command_configure_adfsbuffers(const char *buffers);
#endif
_kernel_oserror *command_configure_syntax_adfsdircache(void);
_kernel_oserror *command_status_adfsdircache(void);
_kernel_oserror *command_syntax_adfsdircache(void);
_kernel_oserror *command_configure_adfsdircache(const char *dircache);
_kernel_oserror *command_configure_syntax_drive(void);
_kernel_oserror *command_status_drive(void);
_kernel_oserror *command_syntax_drive(void);
_kernel_oserror *command_configure_drive(const char *drive);

#endif
